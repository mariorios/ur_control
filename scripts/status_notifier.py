#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy

# Author: Andrew Dai
# This ROS Node converts Joystick inputs from the joy node
# into commands for turtlesim

# Receives joystick messages (subscribed to Joy topic)
# then converts the joysick inputs into Twist commands
# axis 1 aka left stick vertical controls linear speed
# axis 0 aka left stick horizonal controls angular speed
global pub
global pub2

def callback(data):
    vel = Twist()

    # Left joystick
    vel.linear.x = 0.458*data.axes[1]
    vel.angular.z = 0.458*data.axes[0]

    # XPAD - only send xpad speed
    if vel.linear.x == 0 and vel.angular.x == 0:
        vel.linear.x = 0.085 * data.axes[7]
        vel.angular.z = 0.070 * data.axes[6]
        pub.publish(vel)
        pub2.publish(vel)



# Intializes everything
if __name__ == '__main__':
    rospy.init_node('status_notifier')

    pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
    pub2 = rospy.Publisher('turtle1/cmd_vel', Twist)
    # subscribed to joystick inputs on topic "joy"
    rospy.Subscriber("/odom", Joy, callback)
    #
    # rate = rospy.Rate(10)
    # while not rospy.is_shutdown():
    #     pub.publish(vel)
    #     pub2.publish(vel)
    #     rate.sleep()
    rospy.spin()