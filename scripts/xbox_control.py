#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from ur_msgs.msg import Start

from yaml import load
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

# Author: Andrew Dai
# This ROS Node converts Joystick inputs from the joy node
# into commands for turtlesim

# Receives joystick messages (subscribed to Joy topic)
# then converts the joysick inputs into Twist commands
# axis 1 aka left stick vertical controls linear speed
# axis 0 aka left stick horizonal controls angular speed
global pub
global pub2
global pub3

started = False
start_cmd = None

def callback(data):
    vel = Twist()

    # Left joystick
    vel.linear.x = 0.300*data.axes[1]
    vel.angular.z = 0.300*data.axes[0]

    # XPAD - only send xpad speed
    if vel.linear.x == 0 and vel.angular.x == 0:
        vel.linear.x = 0.085 * data.axes[7]
        vel.angular.z = 0.070 * data.axes[6]
        pub.publish(vel)
        pub2.publish(vel)
    global started
    if data.buttons[0] == 1:
        if not started:
            pub3.publish(start_cmd)
            # started = True


# Intializes everything
if __name__ == '__main__':
    with open('/Alan/config.yaml', 'r') as y:
        data = load(y, Loader=Loader)
    if data is None:
        raise ValueError

    start_cmd = Start()
    color = 0
    if data['color'] == 'red':
        color = Start.RED
    if data['color'] == 'blue':
        color = Start.BLUE
    else:
        raise ValueError

    start_cmd.color = color
    start_cmd.cars = data['Cars']

    rospy.init_node('ur_xbox_control')

    pub = rospy.Publisher('cmd_vel', Twist, queue_size=1)
    pub2 = rospy.Publisher('turtle1/cmd_vel', Twist)
    pub3 = rospy.Publisher('start', Start)
    # subscribed to joystick inputs on topic "joy"
    rospy.Subscriber("joy", Joy, callback)
    #
    # rate = rospy.Rate(10)
    # while not rospy.is_shutdown():
    #     pub.publish(vel)
    #     pub2.publish(vel)
    #     rate.sleep()
    rospy.spin()
